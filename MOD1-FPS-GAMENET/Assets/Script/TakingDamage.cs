using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
public class TakingDamage : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update

    public float startHealth = 100;

    public float health;

    [SerializeField]
    Image slideBar;

    void Start()
    {
        health = startHealth;
        slideBar.fillAmount = health / startHealth;
    }

  
    [PunRPC]
    public void TakeDamage(int dmg)
    {
        health -= dmg;

        slideBar.fillAmount = health / startHealth;
        if (health < 0)
        {
            Die();
        }
    }

    public void Die()
    {
        if (photonView.IsMine)
        {
            GameManager.instance.LeaveRoom();
        }
      
    }
}
